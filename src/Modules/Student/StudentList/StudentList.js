import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './StudentList.css';

const BASE_URL = 'http://localhost:8080'

class StudentList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            studentList: []
        }
    }

    componentDidMount() {
        this.fetchStudentList();
    }

    fetchStudentList() {
        fetch(`${BASE_URL}/users`)
            .then(response => response.json())
            .then(json => {
                
                console.log(json)
                this.setState({
                    studentList: json
                })
            })
    }

    handleDelete = (userId) => {
        /* fetch('https://jsonplaceholder.typicode.com/users' + rollNo, { */
        fetch(`{BASE_URL}/user/${userId}`, {
            method: 'DELETE',
        })
        .then(res => {
            console.log(res);
            if(res.ok) {
                alert(`Deleted ${userId} successfully`)
                this.fetchStudentList();
            } else {
                alert("error!!")
            }
         } )
        

    }
    render() {
        const {studentList} = this.state;
        return (
            <table>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                {studentList.map((student = {}) => {
                    const { phoneNumber, userId, name, email, gender } = student;
                    return (
                        <tr>
                            <td>{userId}</td>
                            <td>{name}</td>
                            <td>{phoneNumber}</td>
                            <td>{email}</td>
                            <td>{gender}</td>
                            <td>
                                <Link to={`/edit/${userId}`}><img src="https://img.icons8.com/wired/64/000000/edit.png" /></Link>
                            </td>
                            <td onClick={() => this.handleDelete(userId)}>
                               <Link to="#"><img src="https://img.icons8.com/carbon-copy/100/000000/delete-forever--v1.png" /></Link>
                            </td>
                        </tr>
                    )
                })}
            </table>
        )

    }
}

export default StudentList;